var data = [
    {
        "name": "HTML CSS JS",
        "url": "html.png"
    },
    {
        "name": "React",
        "url": "react.png"
    },
    {
        "name": "Google Polymer",
        "url": "polymer.png"
    },
    {
        "name": "Git",
        "url": "git.png"
    },
    {
        "name": "Python",
        "url": "python.jpeg"
    },
    {
        "name": "Android",
        "url": "android.jpg"
    }
];

console.log(data.length);