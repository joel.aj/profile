var dataFileURL = "assets/js/data.json"
var $containerDiv;
$(document).ready(function () {
    $containerDiv = $("#item-container");
    var itemTemplateRawHTML = $("#item-template").text();
    console.log(itemTemplateRawHTML);
    var items = data;
    items.forEach((item) => {
        var itemRawHTML = itemTemplateRawHTML.replace("{imageURL}", "assets/img/" + item.url);
        itemRawHTML = itemRawHTML.replace("{itemName}", item.name);
        itemRawHTML = itemRawHTML.replace("{alt}",item.name);
        console.log(itemRawHTML);
        $containerDiv.append($.parseHTML(itemRawHTML));
    });
})